import { MwaPage } from './app.po';

describe('mwa App', () => {
  let page: MwaPage;

  beforeEach(() => {
    page = new MwaPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});

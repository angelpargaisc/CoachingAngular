import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  moduleSelected = ""
  showView = true
  menu = [
    {
      module: 'Components',
      title: 'Markup',
      path: '/components1',
      example: {
        title: "Components",
        component: "Components/components-1-markup",
        description: `You should create one views inside the Components/components-1-markup folder, every view should be in Components/components-1-markup/views.
create a navbar and a footer that should be render as global components inside your view
        `
      }
    },
    {
      module: 'Components',
      title: 'Routing',
      path: '/components2',
      example: {
        title: "Routing",
        component: "Components/components-2-routing",
        description: `Replicate what was done in the previous exercise, and additional add one more view
you should create a routing that works as a sub routing of /components2/[yourView]
Note: remember that the footer and navbar should be global components`,
      }
    },
    {
      module: 'Communication',
      title: 'Inheritance Component',
      path: '/communication1',
      example: {
        title: "Inheritance Component",
        component: "Communication/communication-1-inheritance",
        description: `In the communication-1-inheritance component it is necessary to build a communication bridge between the search input within this component and its child list 
        you should be able to filter the list`
      }
    },
    {
      module: 'Communication',
      title: 'Inputs/Outputs',
      path: '/communication2',
      example: {
        title: "Communication with Inputs/Outputs",
        component: "Communication/communication-2-io",
        description: `In the communication-2-IO component it is necessary to build a communication bridge between the search component and its parent to propagate an entry from the search filter, also is required to have a communication (bindig) from the parent with the list componet
        you should be able to filter the list`
      }
    },
    {
      module: 'Communication',
      title: 'Service',
      path: '/communication3',
      example: {
        title: "Communication with Service",
        component: "Communication/communication-3-service",
        description:`you should be able to filter the list using a service to communicate both components`
      }
    },
    {
      module: 'Forms',
      title: 'Template approach',
      path: '/forms1',
      example: {
        title: "Forms (Template approach)",
        component: "Forms/forms-1-template",
        description: `Is required build a Form since the Template approach with the validations described on the objective
    
        Name - required
        User Name - required & minlength>=5
        email - required y pattern (email)
        password - requerido & pattern (letter >= 4, numbers >= 1, special characters >= 1)
        password confirmation - required & validate that is the same with password
        birthday - required & pattern (mm/dd/aaaa)
        sex - optional
        2 security questions (and answers obviously) - required & user should not be able to select the same twice
        `

      }
    },
    {
      module: 'Forms',
      title: 'Data approach',
      path: '/forms2',
      example: {
        title: "Forms (Data approach)",
        component: "Forms/forms-2-data",
        description: `Is required build a Form since the Data approach with the validations described on the objective

        Name - required
        User Name - required & minlength>=5
        email - required y pattern (email)
        password - requerido & pattern (letter >= 4, numbers >= 1, special characters >= 1)
        password confirmation - required & validate that is the same with password
        birthday - required & pattern (mm/dd/aaaa)
        sex - optional
        2 security questions (and answers obviously) - required & user should not be able to select the same twice
        `

      }
    },
    {
      module: 'Directives',
      title: 'Attribute',
      path: '/directives1',
      example: {
        title: "Use of directives and Custom directives",
        component: "Directives/directives-1-attr",
        description: `create a custom directive that highlights a label with a selected value
        User should be able to seletc two colors from two dropdowns (every dropdown should contain at least 5 diferent color options), one of those selections will represent the mouseOver highlight, and the other one the mouseLeave highlight`
      }
    },
    {
      module: 'Directives',
      title: 'Structural',
      path: '/directives2',
      example: {
        title: "Structural Directive",
        component: "Directives/directives-2-structural",
        description: `create an structural directive that let us hide or show a content depending of a condition
        user should be able to use this directive and hide/show the content inside the tag`
      }
    },
    {
      module: 'Pipes',
      title: 'Native',
      path: '/pipes1',
      example: {
        title: "Application of pipes",
        component: "Pipes/pipes-1-native",
        description: `according with the provided array, you should render every value with the requested way`
      }
    },
    {
      module: 'Pipes',
      title: 'Custom',
      path: '/pipes2',
      example: {
        title: "Custom Pipes",
        component: "Pipes/pipes-2-custome",
        description: `create a custom pipe to recognize a name inside a string
        you should be able to take every message from the messages array and write with uppercase every name contained inside the string, and write with lowercase the other words. Note: names must be requested as a parameter, example: {{message |customepipe: 'Angel'}}`
      }
    },
    {
      module: 'Services',
      title: 'CRUD',
      path: '/services',
      example: {
        title: "Services (CRUD)",
        component: "Services/services-crud",
        description: `To success here you will need to create a CRUD app for the next data structure
                  
              Name - required
              User Name - required & minlength>=5
              email - required y pattern (email)
              password - requerido & pattern (letter >= 4, numbers >= 1, special characters >= 1)
              password confirmation - required & validate that is the same with password
              birthday - required & pattern (mm/dd/aaaa)
              sex - optional
              2 security questions (and answers obviously) - required & user should not be able to select the same twice

        Use firebase to the data base
        use a service to communicate the app with firebase
        use form validation
        build a routing navigation to navigate on your CRUD app
        use observables
        use at least one custom directives
        use at least one pipe and one custom pipe
        
        `,
      }
    },
  ]
  example
  toogleShowView() {
    this.showView = !this.showView
  }
  toggleModule(module) {
    this.moduleSelected = module
  }


  get modules() {
    return this.menu.map(function (a) { return a.module; }).filter((value, index, self) => self.indexOf(value) === index);
  }
  menuByModule(module) {

    return this.menu.filter(option => option.module == module);
  }
  setExample(example){
    this.example=example
  }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes-2-custome',
  templateUrl: './pipes-2-custome.component.html',
  styleUrls: ['./pipes-2-custome.component.css']
})
export class Pipes2CustomeComponent implements OnInit {
  messages=[
    "My namE is angel aNd I am A SoftWare engIneer",
    "joel Is hAving ProBlemS Whith ThIs Exercise",
    "JhON, SMitH, PablO and TerEsa are iN THE meeting",
  ]
  constructor() { }

  ngOnInit() {
  }

}

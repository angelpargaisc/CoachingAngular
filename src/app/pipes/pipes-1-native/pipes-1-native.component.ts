import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes-1-native',
  templateUrl: './pipes-1-native.component.html',
  styleUrls: ['./pipes-1-native.component.css']
})
export class Pipes1NativeComponent implements OnInit {
  list=[
    {value:new Date("01/01/2018"),expectedResult:"Jan 1, 2018"},
    {value:new Date("01/01/2018"),expectedResult:"01/01/2018"},
    {value:3.1415927,expectedResult:"3.14159"},
    {value:0.12345,expectedResult:"$0.12"},
    {value:"My name is Paul Shan",expectedResult:"my name is paul shan"},
    {value:"My name is Paul Shan",expectedResult:"MY NAME IS PAUL SHAN"},
    {value:{ name: {fName: "Paul", lName:"Shan"}, site:"VoidCanvas", luckyNumbers:[7,13,69] },expectedResult:'{ "name": { "fName": "Paul", "lName": "Shan" }, "site": "VoidCanvas", "luckyNumbers": [ 7, 13, 69 ] }'},
    {value:0.1415927,expectedResult:"014.16%"},
    {value:"voidcanvas.com",expectedResult:""},
  ]

  constructor() { }

  ngOnInit() {
  }

}

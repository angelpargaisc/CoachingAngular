import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-communication-1-inheritance',
  templateUrl: './communication-1-inheritance.component.html',
  styleUrls: ['./communication-1-inheritance.component.css']
})
export class Communication1InheritanceComponent implements OnInit {

  list:Array<string>;
  constructor() { }

  ngOnInit() {
    this.list=[
      "Angel",
      "Jose",
      "Manuel",
      "Eduardo",
      "Ana",
      "Nancy",
      "Elsa",
      "Gabriela",
      "Paola",
      "Alejandro",
      "Raquel"
    ]
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { Components1MarkupComponent } from './components/components-1-markup/components-1-markup.component';
import { Components2RoutingComponent } from './components/components-2-routing/components-2-routing.component';
import { Communication1InheritanceComponent } from './communication/communication-1-inheritance/communication-1-inheritance.component';
import { Communication2IoComponent } from './communication/communication-2-io/communication-2-io.component';
import { Communication3ServiceComponent } from './communication/communication-3-service/communication-3-service.component';
import { Forms1TemplateComponent } from './forms/forms-1-template/forms-1-template.component';
import { Forms2DataComponent } from './forms/forms-2-data/forms-2-data.component';
import { Directives1AttrComponent } from './directives/directives-1-attr/directives-1-attr.component';
import { Directives2StructuralComponent } from './directives/directives-2-structural/directives-2-structural.component';
import { Pipes1NativeComponent } from './pipes/pipes-1-native/pipes-1-native.component';
import { Pipes2CustomeComponent } from './pipes/pipes-2-custome/pipes-2-custome.component';
import { ServicesCrudComponent } from './services/services-crud/services-crud.component';
import { ListComponent } from './communication/communication-1-inheritance/list/list.component';
import { ListIOComponent } from './communication/communication-2-io/list/list.component';
import { SearchIOComponent } from './communication/communication-2-io/search/search.component';
import { Search3Component } from './communication/communication-3-service/search/search.component';
import { List3Component } from './communication/communication-3-service/list/list.component';


const routes: Routes = [
  //{ path: '', redirectTo: 'home', pathMatch: 'full', },
  { path: 'components1', component: Components1MarkupComponent },
  { path: 'components2', component: Components2RoutingComponent },
  { path: 'communication1', component: Communication1InheritanceComponent },
  { path: 'communication2', component: Communication2IoComponent },
  { path: 'communication3', component: Communication3ServiceComponent },
  { path: 'forms1', component: Forms1TemplateComponent },
  { path: 'forms2', component: Forms2DataComponent },
  { path: 'directives1', component: Directives1AttrComponent },
  { path: 'directives2', component: Directives2StructuralComponent },
  { path: 'pipes1', component: Pipes1NativeComponent },
  { path: 'pipes2', component: Pipes2CustomeComponent },
  { path: 'services', component: ServicesCrudComponent },
];
@NgModule({
  declarations: [
    AppComponent,
    Components1MarkupComponent,
    Components2RoutingComponent,
    Communication1InheritanceComponent,
    Communication2IoComponent,
    Communication3ServiceComponent,
    Forms1TemplateComponent,
    Forms2DataComponent,
    Directives1AttrComponent,
    Directives2StructuralComponent,
    Pipes1NativeComponent,
    Pipes2CustomeComponent,
    ServicesCrudComponent,
    ListComponent,
    ListIOComponent,
    SearchIOComponent,
    Search3Component,
    List3Component
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
